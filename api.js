'use strict';

/*
 * Environment parameters
 * - PORT
 * - DBNAME
 * - DBHOST
 * - DBUSER
 * - DBPASS
 *
 */

const mysql = require("mysql");
const bodyParser = require('body-parser');
const express = require('express');

const port = process.env.PORT || 80;
const app = express();

app.listen(port);

console.log('Cordial DB RESTful API server started on: ' + port);

var sql = require('./db.js');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/health', function (req, res) {
    console.log('GET /health');
    return res.send({ error: false, message: 'Healthy' });
});

var room_reviews = require('./room_reviews.js');
var hotel_reviews = require('./hotel_reviews.js');

app.use('/room_reviews', room_reviews);
app.use('/hotel_reviews', hotel_reviews);
