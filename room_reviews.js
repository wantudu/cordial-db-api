var express = require('express');
var router = express.Router();
var sql = require('./db.js');

/**
 * Business logic for accepting reviews
 */

function isThereAReviewForToday(room, hotel) {
  return new Promise(function(resolve, reject) {
    sql.query("SELECT count(*) FROM room_reviews where room = ? AND hotel = ? AND DATE_FORMAT(created_at, '%Y-%m-%d') = CURDATE()",
    [room, hotel],
    function (error, results) {
      if (error) {
         reject(new Error('Ooops, something broke!'));
      }
      console.log("Results for today: " + results[0]['count(*)']);
      resolve(results[0]['count(*)'] > 0);
    })
  })
}

var Spec = require('bspec').PromiseSpec;

var businessIsThereAReviewForToday = function businessIsThereAReviewForToday(review) {
  return isThereAReviewForToday(review.room, review.hotel)
};

var validReviewSpec = Spec(businessIsThereAReviewForToday).not();

/*
 * Room Review API
 */

router.get('/', function (req, res) {
  console.log('GET /room_reviews');
  sql.query("SELECT * FROM room_reviews order by created_at", function (error, results) {
    if (error) {
       return res.send({ error: true, data: results, message: 'Error in SQL.' });
    }
    console.log('GET /room_reviews: ' + results);
    res.send({ error: false, data: results, message: 'List of room reviews.' });
  });
});

router.get('/forToday', function (req, res) {
  console.log('GET /room_reviews/forToday');
  console.log([req.query.room, req.query.hotel]);
  sql.query("SELECT count(*) FROM room_reviews where room = ? AND hotel = ? AND DATE_FORMAT(created_at, '%Y-%m-%d') = CURDATE()",
  [req.query.room, req.query.hotel],
  function (error, results) {
    if (error) {
       return res.send({ error: true, data: [req.query.room, req.query.hotel], message: 'Error in SQL.' });
    }
    res.send({ error: false, data: results, message: 'Number of reviews for today in that room.' });
  });
});

router.get('/last_review', function (req, res) {
  console.log('GET /room_reviews/last_review');
  console.log([req.query.room, req.query.hotel]);
  sql.query("SELECT * FROM room_reviews where room = ? AND hotel = ? ORDER BY CREATED_AT DESC LIMIT 1",
  [req.query.room, req.query.hotel],
  function (error, results) {
    if (error) {
       return res.send({ error: true, data: [req.query.room, req.query.hotel], message: 'Error in SQL.' });
    }
    res.send({ error: false, data: results, message: 'Number of reviews for today in that room.' });
  });
});

router.post('/', function (req, res) {
    console.log('POST /room_reviews');
    let review = req.body;
    if (!review) {
      console.log('POST /room_reviews: no id');
      return res.status(400).send({ error:true, message: 'Please provide room review' });
    }
    console.log("Review to post: "+ review)

    validReviewSpec.isSatisfiedBy(review)
    .then(function(result) {
      console.log("POST /room_reviews: Business rules meet? " + result)
      if(!result) {
        console.log('POST /room_reviews: Business rules not meet');
        return res.status(400).send({ error:true, message: 'Business rules not meet' });
      }

      sql.query("INSERT INTO room_reviews SET ? ", review, function (error, results, fields) {
        if (error) {
           console.log('POST /room_reviews: SQL Error');
           return res.send({ error: true, data: results, message: 'Error in SQL.' });
        }
        console.log('POST /room_reviews: Success');
        res.send({ error: false, data: results, message: 'New room review has been created successfully.' });
      });
    }).catch(function(err) {
      console.log('POST /room_reviews: Business rules Error');
      res.status(400).send({ error:true, message: 'Business rules error' });
    });

});

router.delete('/', function (req, res) {
  let review_id = req.body.id;
  console.log("DELETE /room_reviews: " + review_id + " " + req.body);
  if(!review_id) {
    return res.status(400).send({ error:true, message: 'Please provide room review id to delete' });
  }
  sql.query("DELETE FROM room_reviews WHERE ID = ? ", [review_id], function (error, results, fields) {
    if (error) {
       return res.send({ error: true, data: review_id, message: 'Error in SQL.' });
    }
    res.send({ error: false, data: review_id, message: 'Room review has been deleted successfully.' });
  });
});

module.exports = router;
