var express = require('express');
var router = express.Router();
var sql = require('./db.js');

/**
 * Business logic for accepting reviews
 */

function isThereAReviewForToday(room, hotel) {
  return new Promise(function(resolve, reject) {
    sql.query("SELECT count(*) FROM hotel_reviews where room_number = ? AND hotel = ? AND DATE_FORMAT(created_at, '%Y-%m-%d') = CURDATE()",
    [room, hotel],
    function (error, results) {
      if (error) {
         reject(new Error('Ooops, something broke!'));
      }
      console.log("Results for today: " + results[0]['count(*)']);
      resolve(results[0]['count(*)'] > 0);
    })
  })
}

var Spec = require('bspec').PromiseSpec;

var businessIsThereAReviewForToday = function businessIsThereAReviewForToday(review) {
  return isThereAReviewForToday(review.room_number, review.hotel)
};

var validReviewSpec = Spec(businessIsThereAReviewForToday).not();

/*
 * Hotel Review API
 */

router.get('/:id', function (req, res) {
   console.log("GET /hotel_reviews/:id")
   let review_id = req.params.id;
   console.log("Params id: " + review_id)

   sql.query("SELECT * FROM hotel_reviews where id = ?", [review_id], function (error, results) {
     if (error) {
        return res.send({ error: true, data: results, message: 'Error in SQL.' });
     }
     return res.send({ error: false, data: results, message: 'Hotel review data.' });
   });
 });

router.get('/', function (req, res) {
  console.log("GET /hotel_reviews")
  sql.query("SELECT * FROM hotel_reviews order by created_at", function (error, results, fields) {
    if (error) {
       return res.send({ error: true, data: results, message: 'Error in SQL.' });
    }
    return res.send({ error: false, data: results, message: 'List of hotel reviews.' });
  });
});

router.post('/', function (req, res) {
    console.log("POST /hotel_reviews")
    console.log(req.body)
    let review = req.body;
    if (!review) {
      return res.status(400).send({ error:true, message: 'Please provide hotel review' });
    }

    validReviewSpec.isSatisfiedBy(review)
    .then(function(result) {
      console.log("POST /hotel_reviews: Business rules meet? " + result)
      if(!result) {
        console.log('POST /hotel_reviews: Business rules not meet');
        return res.status(400).send({ error:true, message: 'Business rules not meet' });
      }

      sql.query("INSERT INTO hotel_reviews SET ? ", review, function (error, results, fields) {
        if (error) {
           return res.send({ error: true, data: results, message: 'Error in SQL.' });
        }
       return res.send({ error: false, data: results, message: 'New hotel review has been created successfully.' });
       });
    }).catch(function(err) {
      console.log('POST /room_reviews: Business rules Error');
      res.status(400).send({ error:true, message: 'Business rules error' });
    });

});

router.delete('/', function (req, res) {
    let review_id = req.body.id;
    if (!review_id) {
      return res.status(400).send({ error:true, message: 'Please provide hotel review' });
    }
   sql.query("DELETE FROM hotel_reviews WHERE ID = ? ", [review_id], function (error, results, fields) {
     if (error) {
        return res.send({ error: true, data: results, message: 'Error in SQL.' });
     }
    return res.send({ error: false, data: results, message: 'New hotel review has been created successfully.' });
    });
});

module.exports = router;
