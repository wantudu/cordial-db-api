create database if not exists cordial_db_api;
use cordial_db_api;

CREATE TABLE IF NOT EXISTS `room_reviews` (
  `id` int(11) NOT NULL,
  `score` integer NOT NULL,
  `text` varchar(1023) NOT NULL,
  `room` varchar(7) NOT NULL,
  `hotel` varchar(63) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `room_reviews` ADD PRIMARY KEY (`id`);
ALTER TABLE `room_reviews` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE IF NOT EXISTS `hotel_reviews` (
  `id` int(11) NOT NULL,
  `room_number` varchar(7) NOT NULL,
  `hotel` varchar(63) NOT NULL,
  `data` varchar(2047) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `hotel_reviews` ADD PRIMARY KEY (`id`);
ALTER TABLE `hotel_reviews` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
