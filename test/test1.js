var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runinng.

const testhost = process.env.TESTHOST
const testport = process.env.TESTPORT

var server = supertest.agent("http://" + testhost + ":" + testport);

// UNIT test begin

describe("Testing health function",function(){

  // #1 should return home page

  it("should return json without errors and a healthy message",function(done){

    // calling home page api
    server
    .get("/health")
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);
      // Error key should be false.
      res.body.message.should.equal("Healthy");
      done();
    });
  });

});
