var supertest = require("supertest");
var should = require("should");

// This agent refers to PORT where program is runinng.

const testhost = process.env.TESTHOST
const testport = process.env.TESTPORT

var server = supertest.agent("http://" + testhost + ":" + testport);

// UNIT test begin

describe("Testing hotel_review post function",function(){

  var review_id;
  var data = {"room_number":"1001","hotel":"PRUEBA","data": JSON.stringify({"a": 1, "b": 2, "c": 3})};
  // #1 should add a review

  it("should return json without errors and a message of inserting review",function(done){


    // calling home page api
    server
    .post("/hotel_reviews")
    .send(data)
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);
      done();
    });
  });

  // #2 should return a list of reviews

  it("should return json without errors and a list of reviews",function(done){

    // calling home page api
    server
    .get("/hotel_reviews")
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);

      res.body.data.should.containDeep([data])
      review_id = res.body.data[0].id
      done();
    });
  });

  // #3 should try to add a review in the same day

  it("should return json with errors and a message about repeated review same day (business rules not meet)",function(done){


    // calling home page api
    server
    .post("/hotel_reviews")
    .send(data)
    .expect("Content-type",/json/)
    .expect(400) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(400);
      res.body.message.should.equal('Business rules not meet');
      done();
    });
  });

  // #4 should return a message with the data for review_id

  it("should return json without errors and the review contents",function(done){

    // calling home page api
    server
    .get("/hotel_reviews/" + review_id)
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);
      res.body.data.should.containDeep([data])
      done();
    });
  });

  // #4 should return a message of removing review

  it("should return json without errors and removed the review",function(done){

    // calling home page api
    server
    .delete("/hotel_reviews")
    .send({id: review_id})
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);
      done();
    });
  });

  // #5 should return a message of error because review is not in the database

  it("should return json with error about no review found",function(done){

    // calling home page api
    server
    .get("/hotel_reviews/" + review_id)
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.error.should.equal(false);
      res.status.should.equal(200);

      res.body.data.should.be.empty()
      done();
    });
  });

});
