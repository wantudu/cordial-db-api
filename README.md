* README

Api to connect a MySQL DB to store room and hotel reviews.

* Functions

- Application for client cordial
- CRD for room_reviews
- search for a room review of a given room in current date
- search for the last room review of a given room
- CRD for hotel_reviews

* Development

Run local server in nodemon (bind to localhost:4000)
- $ npm run dev

* Test

** Local

* Local mysql database in 3306 (localhost, root, password, cordial_db_api)
* Import db.sql database
* $ npm run dev
* $ npm run mocha

** Full

Pre:
 - docker
 - docker-compose

Run tests:
 - npm test
