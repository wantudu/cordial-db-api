pipeline {
    agent {
      docker {
        image "wantudu/jenkins-agent-npm:latest"
        args '-u root:root --tmpfs /.config -v /var/run/docker.sock:/var/run/docker.sock'
      }
    }
    stages {
        stage('source') {
            steps {
                git(
                   url: 'https://bitbucket.org/wantudu/cordial-db-api.git',
                   credentialsId: 'dsuarez-bitbucket',
                   branch: "master"
                )
            }
        }
        stage('build') {

            steps {
                script {
                    def customImage = docker.build("wantudu/cordial-db-api")
                    docker.withRegistry('https://293132088428.dkr.ecr.eu-west-1.amazonaws.com', 'ecr:eu-west-1:jenkins-aws-credentials') {
                      customImage.push('latest')
                    }
                }
            }
        }
        stage('deploy') {
            steps {
                withAWS(credentials:'jenkins-aws-credentials', region:'eu-west-1') {
                  sh label:"Setup task definition and uploading artifacts to S3", script:'''
                  #!/bin/bash
                  CLUSTER="WebApp"
                  SERVICE_NAME="CordialDBApiService"
                  IMAGE_VERSION="latest"
                  REGION="eu-west-1"
                  TASK_FAMILY="CordialDBApiTask"
                  ACCOUNTID="293132088428"
                  TASK_ARN="arn:aws:ecs:${REGION}:${ACCOUNTID}:task-definition"
                  IMAGE_URI="293132088428.dkr.ecr.eu-west-1.amazonaws.com/wantudu/cordial-db-api:latest"

                  # Create a new task definition for this build
                  aws ecs register-task-definition --family ${TASK_FAMILY} --cli-input-json file://taskdef.json

                  # Get the task revision
                  TASK_REVISION=`aws ecs describe-task-definition --task-definition ${TASK_FAMILY} | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | sed 's/,$//'`

                  # Generate the new appspec with the task revision. It is ready to be uploaded to artifacts directory in S3
                  sed -e "s,<TASK_DEFINITION>,${TASK_ARN}/${TASK_FAMILY}:${TASK_REVISION}," appspec.yaml > appspec-${TASK_REVISION}.yaml

                  # Update artifacts to S3
                  aws s3 mb s3://cordial-db-api-deployment
                  aws s3 cp appspec-${TASK_REVISION}.yaml s3://cordial-db-api-deployment/appspec.yaml

                  '''
                }


                withAWS(credentials:'jenkins-aws-credentials', region:'eu-west-1') {
                    sh label:"Launching CodeDeploy and waiting for it to finish", script:'''
                    #!/bin/bash +e
                    TARGETID="WebApp:CordialDBApiService"

                    DEPLOYID=`aws deploy create-deployment --cli-input-json file://create-deployment.json | awk '{print $2}' | tr -d "\n" | sed 's/"//g'`

                    # Wait for the deployment is completed
                    STATUS="Running"
                    while [ "${STATUS}" != "Succeeded" ]; do
                      sleep 10
                      STATUS=`aws deploy get-deployment-target --deployment-id "${DEPLOYID}" --target-id ${TARGETID} | jq -r ".deploymentTarget.ecsTarget.status"`
                      echo "Retrieve current deployment status: ${STATUS}\n"
                      if [ "${STATUS}" == "FAILED" ] || [ $? == 0 ]; then
                          echo "Failed. There was an error\n"
                          break
                      fi
                    done

                    '''
                }


            }
        }
    }
    post {
        always {
            withAWS(credentials:'jenkins-aws-credentials', region:'eu-west-1') {
                sh label:"Cleaning artifacts from S3", script:'''
                #!/bin/bash
                # Clean
                aws s3 rm s3://cordial-db-api-deployment/appspec.yaml
                aws s3 rb s3://cordial-db-api-deployment
                rm appspec-*.yaml

                '''
            }
            cleanWs()
        }
        success {
            echo 'Success'
        }
        failure {
            echo 'Failure'
        }
        unstable {
            echo 'Unstable'
        }
        changed {
            echo 'Pipeline status changed.'
        }
    }
}
